﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIManager : Singleton<UIManager> {

    public Text GamePlayScore;
    public Text MainMenuScore;
    public RectTransform[] Canvases;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void UpdateScore()
    {
        GamePlayScore.text = GameManager.Instance.score.ToString();
    }

    public void ShowMainMenu()
    {

        Canvases[1].gameObject.SetActive(false);
        if (GameManager.Instance.score <= 0)
            MainMenuScore.text = "";
        else
            MainMenuScore.text = GamePlayScore.text;
        Canvases[0].gameObject.SetActive(true);
    }

    public void ShowGamePlayMenu()
    {

        Canvases[0].gameObject.SetActive(false);

        Canvases[1].gameObject.SetActive(true);
    }

    public void ToggleTutorialPanel(bool value)
    {
        Canvases[2].gameObject.SetActive(value);
    }
}
