﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pauser : Singleton<Pauser> {

    public bool paused;
	
    void Start()
    {
        TogglePause();
    }

	void Update () {
	    if(Input.GetKeyDown(KeyCode.P))
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        paused = !paused;
        if (paused)
        {
            Time.timeScale = 0.1f;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
