﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {

    public ObjectPoolName PoolName;
    public GameObject PrefabToPool;
    public int maxObjects;
    
    public bool isUsed
    {
        get
        {
            return Count < maxObjects;
        }
    }

    public int Count
    {
        get
        {
            return objectPool.Count;
        }
    }

    List<GameObject> objectPool;
    void Start()
    {
        objectPool = new List<GameObject>();
        for (int i = 0; i < maxObjects; i++)
        {
            GameObject pooledObject = (GameObject)Instantiate(PrefabToPool);
            pooledObject.transform.name = PrefabToPool.transform.name + ":" + i;
            pooledObject.transform.SetParent(transform);
            objectPool.Add(pooledObject);
            pooledObject.SetActive(false);
        }
    }

    public GameObject Instantiate()
    {
        if (objectPool.Count == 0)
            return null;
        GameObject objectToReturn = objectPool[0];
        objectPool.Remove(objectToReturn);
        objectToReturn.SetActive(true);
        return objectToReturn;
    }

    public void Destroy(GameObject objectToDestroy)
    {
        objectToDestroy.SetActive(false);
        objectPool.Add(objectToDestroy);
    }
}
