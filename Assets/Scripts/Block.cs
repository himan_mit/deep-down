﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    Block botBuddy;
    Block topBuddy;

    Transform camKillPoint;
    Transform camSpawnPoint;

    void Start()
    {
        camKillPoint = GameManager.Instance.camKillPoint;
        camSpawnPoint = GameManager.Instance.camSpawnPoint;
    }

    void Update()
    {
        if (topBuddy == null && transform.position.y < camKillPoint.position.y)
        {
            topBuddy = ObjectPoolManager.Instance.ObjectPools[ObjectPoolName.BlockPool].Instantiate().GetComponent<Block>();
            topBuddy.transform.position = new Vector3(transform.position.x, transform.position.y + 2.17f, transform.position.z);
            topBuddy.transform.localRotation = Quaternion.Euler(-90, Random.Range(0, 360), 0);
            topBuddy.botBuddy = this;
            //topBuddy.transform.parent = transform.parent;
        }
        if (botBuddy == null && transform.position.y > camSpawnPoint.position.y)
        {
            botBuddy = ObjectPoolManager.Instance.ObjectPools[ObjectPoolName.BlockPool].Instantiate().GetComponent<Block>();
            botBuddy.transform.position = new Vector3(transform.position.x, transform.position.y - 2.17f, transform.position.z);
            botBuddy.transform.localRotation = Quaternion.Euler(-90, Random.Range(0, 360), 0);
            botBuddy.topBuddy = this;
            //botBuddy.transform.parent = transform.parent;
        }
        if (botBuddy != null && botBuddy.transform.position.y > camKillPoint.position.y)
        {
            topBuddy = null;
            botBuddy = null;
            ObjectPoolManager.Instance.ObjectPools[ObjectPoolName.BlockPool].Destroy(gameObject);
        }
    }
}
