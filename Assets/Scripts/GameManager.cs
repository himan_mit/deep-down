﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager> {

    public int score;

    public float currentRunTime = 0;

    public Transform camKillPoint;
    public Transform camSpawnPoint;
    public Player Player;
    bool firstTimePlay = true;

    // Use this for initialization
    void Start ()
    {
        camKillPoint = Camera.main.GetComponent<CameraMover>().CamKillPoint;
        camSpawnPoint = Camera.main.GetComponent<CameraMover>().CamSpawnPoint;
        InvokeRepeating("UpdateScore", 1, 1);
	}
	
	// Update is called once per frame
	void Update () {
        currentRunTime += Time.deltaTime;

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
	}

    public void UpdateScore()
    {
        score = (int)currentRunTime;
        UIManager.Instance.UpdateScore();
    }

    public void SetScore(int value)
    {
        score = value;
        UIManager.Instance.UpdateScore();
    }

    public void GameOver()
    {
        UIManager.Instance.ShowMainMenu();
        Pauser.Instance.TogglePause();
        Player.Reset();
    }

    public void StartGame()
    {
        if (!firstTimePlay)
            Camera.main.GetComponent<CameraMover>().cameraMoveSpeed = 2;
        if (firstTimePlay)
        {
            UIManager.Instance.ToggleTutorialPanel(true);
            firstTimePlay = false;

        }
        Invoke("HideTutorialPanel", 3);
        currentRunTime = 0;
        SetScore(0);
        UIManager.Instance.ShowGamePlayMenu();
        Pauser.Instance.TogglePause();

    }


    public void HideTutorialPanel()
    {
        UIManager.Instance.ToggleTutorialPanel(false);
        Camera.main.GetComponent<CameraMover>().cameraMoveSpeed = 2;
    }

}
