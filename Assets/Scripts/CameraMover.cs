﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

    public Transform CamKillPoint;
    public Transform CamSpawnPoint;

    public float cameraMoveSpeed;
    public float camSpeedIncrement = 0.0005f;
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y - cameraMoveSpeed * Time.deltaTime, transform.position.z);

	}

    void FixedUpdate()
    {
        cameraMoveSpeed += (camSpeedIncrement);
    }
}
