﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ObjectPoolName
{
    BlockPool,
}

public class ObjectPoolManager : Singleton<ObjectPoolManager>
{

    public Dictionary<ObjectPoolName, ObjectPool> ObjectPools;
    // Use this for initialization
    void Start()
    {
        ObjectPools = new Dictionary<ObjectPoolName, ObjectPool>();
        ObjectPool[] objectPools = GetComponentsInChildren<ObjectPool>();

        foreach (ObjectPool objectPool in objectPools)
        {
            ObjectPools.Add(objectPool.PoolName, objectPool);
        }
    }
}