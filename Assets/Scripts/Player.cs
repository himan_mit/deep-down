﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public Transform Environment;

    public float rotationSpeed;

    public float ballMoveSpeed;

    Transform camKillPoint;
    Transform camSpawnPoint;

    Vector3 startPos;

    float screenWidth;
    void Start()
    {
        startPos = transform.position - Camera.main.transform.position;

        camKillPoint = GameManager.Instance.camKillPoint;
        camSpawnPoint = GameManager.Instance.camSpawnPoint;
        //screenWidth = Camera
    }

	void Update ()
    {
#if UNITY_STANDALONE
        //GetComponent<Rigidbody>().velocity = new Vector3(0, ballMoveSpeed * Time.deltaTime, 0);
        if (Input.GetAxis("Horizontal") > 0)
        {
            transform.RotateAround(new Vector3(0, 0,0), new Vector3(0, 1, 0), rotationSpeed * Time.deltaTime);
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            //Environment.Rotate(new Vector3(0, -rotationSpeed * Time.deltaTime, 0));
            transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), -rotationSpeed * Time.deltaTime);
        }

#endif
        if(Input.touchCount > 0)
        {
            if ((Input.GetTouch(0).position.x > Screen.width / 2)) 
            {
                transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), -rotationSpeed * Time.deltaTime);
            }
            else if ((Input.GetTouch(0).position.x <= Screen.width / 2))
            {
                transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), rotationSpeed * Time.deltaTime);
            }
        }

        if (transform.position.y > camKillPoint.position.y || transform.position.y < camSpawnPoint.position.y - 5)
            GameManager.Instance.GameOver();

    }

    public void Reset()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        transform.position = Camera.main.transform.position + startPos;
    }
}
